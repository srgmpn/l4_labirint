Labirint
Arhitectura proiect

*   Mediu de dezvoltare
*   Implementare
*   Git

1. Mediu de dezvoltare
    - Limbajul de programare Java 1.8
    - Mediu de dezvoltare Intellij IDEA
    - Maven

2. Implementare

    2.1 Arhitectura aplicatiei
        Modelul si Vizualizarea datelor au fost separarate in pachete separate, la fel si calcularea
        datelor astfel sa incercat implementare modelului mvc (model-controller- view), fiecare
        componenta are implementata cite o interfata cu metode caracteristice pentru tipul sau
        La fel este creata o interfata care se ocupa cu procesarea solutiilor obtinute, utilizindu-se
        patternul observer pentru a semnala aparitie unei noi solutii.

    2.2 Model
        Interfata principala a modelului este LabyrinthModel, este implementata de clasa abstracta
        AbstractLabyrinthModel - care implementeaza metodele comune pentru toate tipurile de implementari
        concrete, clasa data este mostenita de 2 clase, care propun 2 tipuri diferite de obinere a datelor
        modelului, clasa FileUploadLaybirinthModel presupune citirea datelor dintr-n fisier dupa structura
        prima linie - nr de linii si nr de coloane, pe urmatoare le nr linii se va alfla o matrice,
        compusa din 0 si 1, unde apare -1- punct de start si 2 - punct de finis, pe ultimele 2 linii
        se va afla coordonate punctului de start si coordonatele pucntului de finis
        A doua implementare RandomLabyrinthModel este generarea aleatoare a labirintului, care primeste nr
        de linii si nr de coloane ca parametru.

    2.3 View
        LabyrinthView - propune spre implementare metode pentru afisare a datelor, are 2 implementari
        DLabyrinthView - afisare datelor in forma #-perete .-loc liber, S- start, F- finish
        StarLabyrinthView - afisare datelor in forma *-perete -loc liber, S- start, F- finish

    2.4 solver
        LabyrinthSolver -  interfata care propune spre implementare metode pentru gasirea drumurilor din labirint
        AbstractLabyrinthSolver - clasa abstracta implementeaza interfata LabyrinthSolver, implementeaza attach()
        0 care este comuna pentru toate clasele care extind clasa data, la fel contine diferite metode si variabile
        comune pentru toate tipurile de implementari.
        BackLabyrintSolver - extinde clasa AbstractLabyrinthSolver, astfel implmeneteaza metodele interfetei
        neimplementate in clasa extinsa, aceasta clasa propune ca metoda de rezolvare a labirintului un algoritm
        backtraking, de rezolvare care va genera toate solutiile posibile de orice lungime, (problema acestei
        implmentari este ca este ineficienta din punct de vedere a timpului), care este cea mai ineficienta tehnica,
        dar pentru a obtine toate solutiile, alta implementare nu exista.
        LeeLabyrinthSolver - la fel extinde clasa AbstractLabyrinthSolver, in schimb clasa data propune o
        implementare a unui algoritm de programare dinamica, algoritmul Lee, care are un timp de executie maxim n*m,
        cu ajutorul lui obtinem o matrice de unde putem sa gasim drumul de lungime minima S-F, astfel pentru a putea
        obtine toate drumurile minime am recurs iarasi la u algritm backtraking, mult mai optim decit in prima situatie,
        dar pe matrice obtinuta in urma aplicarii algoritmului Lee.
        ManualLabyrinthSolver - propune o implementare manuala, adica utilizatorul face fiecare pas, este prevazut o
        verificare regex a fiecarei comenzi, la fel si verifica daca exista solutie la pasul urmator pentru a stii daca
        exista continuare, nu permite de aface pasi inapoi, la fel semnifica prin mesaje greselile de directie alese
        (Is Wall, Cant't back, Command Error) comenzile care se pot aplica d-(down), u - (up), l - (left), r - (right),
        exit - iesire


   3. Git
   Pentru version control, am utilizat git.
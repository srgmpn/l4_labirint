package com.srg.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RandomLabyrinthModelTest {
    RandomLabyrinthModel model = new RandomLabyrinthModel(6, 6);;
    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
        model = null;
    }

    @org.junit.Test
    public void testGenerateLabyrinth() throws Exception {
        //model.generateLabyrinth();
        //model.showL();
    }


    public void testGetRowCount() throws Exception {
        assertEquals(model.getRowCount(), 6);
    }

    @org.junit.Test
    public void testGetColumnCount() throws Exception {
        assertEquals(model.getColumnCount(), 6);
    }

    @org.junit.Test
    public void testIsFreeAt() throws Exception {

        assertTrue(model.isFreeAt(0, 1));
    }

    @org.junit.Test
    public void testIsWallAt() throws Exception {
        assertTrue(model.isWallAt(0, 3));
    }

    @org.junit.Test
    public void testGetStartCell() throws Exception {

    }

    @org.junit.Test
    public void testGetFinishCell() throws Exception {

    }

}
package com.srg.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 5/13/15.
 */
public abstract class AbstractLabyrinthModel implements LabyrinthModel{

    protected int [][] model;
    protected int rowCount;
    protected int colCount;
    protected int startx;
    protected int starty;
    protected int stopx;
    protected int stopy;

    public AbstractLabyrinthModel(){
        this(0, 0);
    }

    public AbstractLabyrinthModel(int row, int col){
        this.colCount = col;
        this.rowCount = row;

        model = new int[rowCount][colCount];
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return colCount;
    }

    @Override
    public boolean isFreeAt(int row, int col) {
        return (model[row][col] == 0);
    }

    @Override
    public boolean isWallAt(int row, int col) {
        return (model[row][col] == 1);
    }

    @Override
    public Map<String, Integer> getStartCell() {
        Map<String, Integer> startcell = new HashMap<String, Integer>();
        startcell.put("startx", startx);
        startcell.put("starty", starty);
        return startcell;
    }

    @Override
    public Map<String, Integer> getFinishCell() {
        Map<String, Integer> stopcell = new HashMap<String, Integer>();
        stopcell.put("stopx", stopx);
        stopcell.put("stopy", stopy);
        return stopcell;
    }

    @Override
    public int getValueAt(int row, int col){
        return model[row][col];
    }
}

package com.srg.model;

import com.srg.core.Model;

/**
 * Created by administrator on 5/15/15.
 */
public class LabyrinthModelFactory {

    public static LabyrinthModel getInstance(Model model, int rows, int cols){
        switch (model){
            case FILE:
                return new FileUploadLaybirinthModel();
            case RANDOM:
                return new RandomLabyrinthModel(rows, cols);
        }

        return null;
    }
}

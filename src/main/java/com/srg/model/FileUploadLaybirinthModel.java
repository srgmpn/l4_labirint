package com.srg.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by administrator on 5/13/15.
 */
public class FileUploadLaybirinthModel extends AbstractLabyrinthModel {

    static final String FILE_PATH = "models/model.in";

    public FileUploadLaybirinthModel(){
        this(null);
    }

    public FileUploadLaybirinthModel(String path){

        if (path == null){
            //get file is resources model.in
            path = getClass().getClassLoader().getResource(FILE_PATH).getFile();
        }
        readDates(path);

    }

    /**
     * read dates is file specified in path
     * @param path - path to file
     */
    private void readDates(String path){

        RandomAccessFile in = null;
        String line;
        int i = 0, j = 0, k = -1;

        try {
            in = new RandomAccessFile(path, "r");
            line = in.readLine();
            //row count col count
            rowCount = getInt(line.substring(0, line.indexOf(' ')));
            colCount = getInt(line.substring(line.indexOf(' ') + 1));
            model = new int[rowCount][colCount];

            //table
            while (i != rowCount){
                line = in.readLine();
                this.parseTableLine(line, i);
                i++;
            }
            //start point
            line = in.readLine();
            startx = getInt(line.substring(0, line.indexOf(' ')));
            starty = getInt(line.substring(line.indexOf(' ') + 1));
            //stop point
            line = in.readLine();
            stopx = getInt(line.substring(0, line.indexOf(' ')));
            stopy = getInt(line.substring(line.indexOf(' ')+1));

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }finally {
            if (in != null) {
                try {
                    in.close();

                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * parse string in Integer value
     * @param s - string
     * @return - parsed value
     */
    private int getInt(String s){
        return Integer.parseInt(s.trim());
    }

    /**
     * parse String line and save date in model
     * @param line - dates
     * @param i - row number
     */
    private void parseTableLine(String line, int i){
        int k = -1, j = 0;
        while ((k = line.indexOf(' ')) > -1){

            model[i][j++] = getInt(line.substring(0, k).trim());
            line = line.substring(k + 1).trim();
        }
        model[i][j] = getInt(line.trim());
    }

}

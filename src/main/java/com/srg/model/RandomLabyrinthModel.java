package com.srg.model;

import java.util.Random;

/**
 * Created by administrator on 5/13/15.
 */
public class RandomLabyrinthModel extends AbstractLabyrinthModel {


    Random random = new Random();

    public RandomLabyrinthModel(){
        this(10, 10);
    }

    public RandomLabyrinthModel(int rowCount, int colCount){
        super(rowCount, colCount);
        this.generateLabyrinth();
    }

    /**
     * generate startpoint stoppoint
     * and generate labyrinth
     */
    private void generateLabyrinth(){

        startx = random.nextInt(100000) % rowCount;
        starty = random.nextInt(100000) % colCount;
        stopx = this.getValue(startx, rowCount);
        stopy = this.getValue(starty, colCount);

        for (int i = 0; i < rowCount; ++i){
            for (int j = 0; j < colCount; ++j){
                model[i][j] = random.nextInt(10000) % 2;
            }
        }
        model[startx][starty] = -1;
        model[stopx][stopy] = 2;

    }

    /**
     *
     * @param val - value which is compared
     * @param v - max value
     * @return - generated value
     */
    private int getValue(int val, int v){
        int k = -1;
        while ((k = random.nextInt(10000) % v) == val){}
        return k;
    }
}

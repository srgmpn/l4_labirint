package com.srg.model;

import java.util.Map;

/**
 * Created by administrator on 5/13/15.
 */
public interface LabyrinthModel {

    public int getRowCount();
    public int getColumnCount();
    public boolean isFreeAt(int row, int col);
    public boolean isWallAt(int row, int col);
    public Map<String, Integer> getStartCell();
    public Map<String, Integer> getFinishCell();
    public int getValueAt(int row, int col);

}

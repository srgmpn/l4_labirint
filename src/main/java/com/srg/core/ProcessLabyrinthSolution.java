package com.srg.core;

/**
 * Created by administrator on 5/15/15.
 */
public class ProcessLabyrinthSolution implements LabyrinthObserver {
    static int count = 0;
    @Override
    public void processSolution(int[] di, int[] dj) {

        int ci = di[0];
        int cj = dj[0];

        StringBuilder sol = new StringBuilder("{ Start, ");

        for (int i = 1; i < di.length-1; i++) {

            if (((ci+1) == di[i]) && (cj == dj[i])){
                sol.append("Down, ");
                ci++;

            } else if (((ci-1) == di[i]) && (cj == dj[i])){
                ci--;
                sol.append("Up, ");

            } else if (((ci) == di[i]) && ((cj+1) == dj[i])){
                cj++;
                sol.append("Right, ");

            } else if (((ci) == di[i]) && ((cj-1) == dj[i])){
                cj--;
                sol.append("Left, ");

            }
        }
        sol.append("Finish }");

        System.out.println("\n" + (++count) + ". " +sol.toString());
    }
}

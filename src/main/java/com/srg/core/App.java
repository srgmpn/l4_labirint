package com.srg.core;

import com.srg.model.LabyrinthModel;
import com.srg.model.LabyrinthModelFactory;
import com.srg.solver.LabyrinthSolver;
import com.srg.solver.LabyrinthSolverFactory;
import com.srg.view.LabyrinthView;
import com.srg.view.LabyrinthViewFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by administrator on 5/13/15.
 */
public class App {

    static Map<String, Model> modelMap = new HashMap<String, Model>(2);
    static Map<String, View> viewMap = new HashMap<String, View>(2);
    static Map<String, Solver> solverMap = new HashMap<String, Solver>(3);

    public static void main(String[] args) {

        setCommandsKeys();

        //create observer
        LabyrinthObserver observer = new ProcessLabyrinthSolution();
        //create model
        System.out.print("__Select type model(FILE, RAND) : ");
        Scanner in  = new Scanner(System.in);
        String command = in.nextLine().trim();
        LabyrinthModel model = LabyrinthModelFactory.getInstance(modelMap.get(command.toLowerCase()), 20, 20);

        System.out.print("__Select type view (D, STAR): ");
        command = in.nextLine().trim();
        LabyrinthView view = LabyrinthViewFactory.getInstance(viewMap.get(command.toLowerCase()), model);

        System.out.print("__Select type solve\n\t     (MAN - manual solve\n" +
                        "\t     LEE - all minim road\n " +
                        "\t     BACK - all variants road " +
                ") : ");
        command = in.nextLine().trim();
        LabyrinthSolver solver = LabyrinthSolverFactory.getInstance(solverMap.get(command.toLowerCase()), model);
        solver.attach(observer);

        System.out.println("\n**** View Labyrinth ****");
        view.show();

        System.out.println("\n**** Solutions ****");

        solver.solve();

        System.out.println("\nProgram finish !");
    }

    static void setCommandsKeys(){
        modelMap.put("file", Model.FILE);
        modelMap.put("rand", Model.RANDOM);

        viewMap.put("d", View.D);
        viewMap.put("star", View.STAR);

        solverMap.put("back", Solver.BACK);
        solverMap.put("lee", Solver.LEE);
        solverMap.put("man", Solver.MAN);
    }
}

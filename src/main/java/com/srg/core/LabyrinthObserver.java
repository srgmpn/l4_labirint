package com.srg.core;

/**
 * Created by administrator on 5/15/15.
 */
public interface LabyrinthObserver {
     public void processSolution(int []di, int dj[]);
}

package com.srg.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by administrator on 5/15/15.
 */
public class RegexVerification {
    private static Pattern pattern;
    private static Matcher matcher;

    private static final String PATTERN = "^((U|u)|(L|l)|(R|r)|(D|d))$";

    /**
     * Validate hex with regular expression
     *
     * @param hex     *            hex for validation
     * @return true valid hex, false invalid hex
     */
    public static boolean validate(final String hex) {
        pattern = Pattern.compile(PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }
}

package com.srg.view;

/**
 * Created by administrator on 5/13/15.
 */
public interface LabyrinthView {
    public void show();
    public String toString();
}

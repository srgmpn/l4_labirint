package com.srg.view;

import com.srg.core.View;
import com.srg.model.LabyrinthModel;

/**
 * Created by administrator on 5/15/15.
 */
public class LabyrinthViewFactory {

    public static LabyrinthView getInstance(View v, LabyrinthModel model){

        switch (v){
            case STAR:
                return new StarLabyrinthView(model);
            case D:
                return new DLabyrinthView(model);
        }

        return null;
    }
}

package com.srg.view;

import com.srg.model.LabyrinthModel;

/**
 * Created by administrator on 5/15/15.
 */
public class DLabyrinthView implements LabyrinthView {
    private LabyrinthModel model;

    public DLabyrinthView(LabyrinthModel model){
        this.model = model;
    }

    @Override
    public void show() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        String l = "";
        for (int i = 0; i < model.getRowCount(); ++i){
            l += "\n";
            for (int j = 0; j < model.getColumnCount(); ++j){

                if (model.isFreeAt(i, j))
                    l += "|.";

                else if (model.isWallAt(i, j))
                    l += "|#";

                else if ( (model.getStartCell().get("startx") == i) &&
                        (model.getStartCell().get("starty") == j))
                    l += "|S";

                else if ( (model.getFinishCell().get("stopx") == i) &&
                        (model.getFinishCell().get("stopy") == j))
                    l += "|F";
            }
            l += "|";
        }
        return l;
    }
}

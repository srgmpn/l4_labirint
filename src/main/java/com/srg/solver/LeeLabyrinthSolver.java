package com.srg.solver;

import com.srg.model.LabyrinthModel;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by administrator on 5/13/15.
 */
public class LeeLabyrinthSolver extends AbstractLabyrinthSolver{

    private int []dx = new int[]{0, 0, -1, 1};
    private int []dy = new int[]{-1, 1, 0, 0};

    private int k = 0, p = 1;

    public LeeLabyrinthSolver(LabyrinthModel model){

        super(model);
        super.border();
    }

    @Override
    public void solve() {
        lee();
        recAllMinimSolution(startx, starty);
    }

    /**
     * algorithm lee
     */
    private void lee(){
        Queue<Integer> x = new ArrayDeque<Integer>();
        Queue<Integer> y = new ArrayDeque<Integer>();

        //set 1 point start
        modelCopy[startx][starty] = 1;

        x.offer(startx);
        y.offer(starty);

        int i, j, inew, jnew;

        while (!x.isEmpty() && !y.isEmpty()){

            i = x.poll();
            j = y.poll();

            for (int dir = 0; dir < 4; dir++ ){

                inew = dx[dir] + i;
                jnew = dy[dir] + j;

                if (nextCellToExplore(inew, jnew)){

                    modelCopy[inew][jnew] = modelCopy[i][j] + 1;
                    x.offer(inew);
                    y.offer(jnew);

                } else {
                    if (isFinish(inew, jnew)){
                        modelCopy[inew][jnew] = modelCopy[i][j] + 1;
                        break;
                    }
                }
            }
        }
    }

    /**
     *  backtraking on result matrix after
     *  lee algorithm
     *  for obtine all solution
     * @param i
     * @param j
     */
    private void recAllMinimSolution(int i, int j){

        if (modelCopy[i][j] == p){
            di[k] = i;
            dj[k] = j;
            k++;
            p++;

            if (isFinish(i, j)){
                notifyAllObservers(k);

            } else {

                recAllMinimSolution(i - 1, j);
                recAllMinimSolution(i, j + 1);
                recAllMinimSolution(i + 1, j);
                recAllMinimSolution(i, j-1);
            }
            p--;
            k--;
        }
    }

    /**
     * verif if can advance next cell
     * @param i - curent row
     * @param j - curent col
     * @return - true if can, else fase
     */
    @Override
    public boolean nextCellToExplore(int i, int j){

        return ( (i >= 1) && (j >= 1) && (i <= nrows) && (j <= ncol) &&
                (modelCopy[i][j] == 0) && (model.isFreeAt(i-1, j-1)));
    }
}

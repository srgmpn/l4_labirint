package com.srg.solver;

import com.srg.core.LabyrinthObserver;

/**
 * Created by administrator on 5/13/15.
 */
public interface LabyrinthSolver {
    public void solve();
    public boolean nextCellToExplore(int i, int j);
    public void attach(LabyrinthObserver observer);
}

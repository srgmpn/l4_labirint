package com.srg.solver;

import com.srg.core.RegexVerification;
import com.srg.model.LabyrinthModel;

import java.util.Scanner;

/**
 * Created by administrator on 5/14/15.
 */
public class ManualLabyrinthSolver extends AbstractLabyrinthSolver{

    private int currx;
    private int curry;
    private int k = 0;
    private String old;
    private boolean b;


    public ManualLabyrinthSolver(LabyrinthModel model){
        super(model);
        currx = startx-1;
        curry = starty-1;
        b = true;
    }

    /**
     * verif if can advance next cell
     * @param i - curent row
     * @param j - curent col
     * @return - true if can, else fase
     */
    @Override
    public boolean nextCellToExplore(int i, int j){

        return ((i >= 0) && (j >= 0) && (i < nrows) &&
                (j < ncol) && (model.isFreeAt(i, j) || isFinish(i+1, j+1)));
    }

    @Override
    public void solve() {

        Scanner in = new Scanner(System.in);

        old = null;
        System.out.println("Commands: d(down) | u(up) | l(left) | r(right) | exit");
        while (!isFinish(currx+1, curry+1) && b) {

            System.out.print("next~> ");
            String command = in.nextLine().trim();

            if (command.equalsIgnoreCase("exit")){
                break;
            }

            if(!futureStep()){
                System.out.println("labyrinth aren't solution!");
                break;
            }

            if (RegexVerification.validate(command)) {

                if (validStep(command))// verif if isn't back step
                    nextStep(command);
                else
                    System.out.println("Can't back!");

            } else {
                System.out.println("Error command");
            }
        }
    }

    /**
     * do next step
     * @param command - next command
     */
    private void nextStep(String command){

        if (futureStep()){ // verif if exist future step

            if (command.equalsIgnoreCase("u") && nextCellToExplore(currx-1, curry)){
                currx--;
                executeCommand(command);

            } else if (command.equalsIgnoreCase("d") && nextCellToExplore(currx+1, curry)){
                currx++;
                executeCommand(command);

            } else if (command.equalsIgnoreCase("l") && nextCellToExplore(currx, curry-1)){
                curry--;
                executeCommand(command);

            } else if (command.equalsIgnoreCase("r") && nextCellToExplore(currx, curry+1)){
                curry++;
                executeCommand(command);

            } else {
                System.out.println("Is Wall");
            }

        } else {
            System.err.println("Not Solution");
            b = false;
        }
    }

    /**
     * save next step
     * @param command - next command
     */
    private void executeCommand(String command){

        di[k] = currx;
        dj[k++] = curry;
        old = command;

        if (isFinish(currx + 1, curry + 1)){
            notifyAllObservers(k);
        }
    }

    /**
     * verif if exist future step
     * @return true if exist, false if not
     */
    private boolean futureStep(){
        //if old is null all 4 dir
        if (old == null){
            return (nextCellToExplore(currx + 1, curry) || nextCellToExplore(currx-1, curry)
                    || nextCellToExplore(currx, curry-1) || nextCellToExplore(currx, curry+1));
        }
        //if old step is up can't do down
        if (old.equalsIgnoreCase("u")) {
            return (nextCellToExplore(currx - 1, curry) ||
                    nextCellToExplore(currx, curry - 1) || nextCellToExplore(currx, curry + 1));
        }
        //if old step is down can't do up
        if (old.equalsIgnoreCase("d")) {
            return (nextCellToExplore(currx + 1, curry) ||
                    nextCellToExplore(currx, curry - 1) || nextCellToExplore(currx, curry + 1));
        }
        //if old step is left can't do rigth
        if (old.equalsIgnoreCase("l")) {
            return (nextCellToExplore(currx + 1, curry) || nextCellToExplore(currx-1, curry)
                    || nextCellToExplore(currx, curry-1));
        }
        //if old step is right can't do left
        if (old.equalsIgnoreCase("r")) {
            return (nextCellToExplore(currx + 1, curry) || nextCellToExplore(currx-1, curry)
                    || nextCellToExplore(currx, curry + 1));
        }
        return false;
    }

    /**
     * verif if next commad is back step
     * @param command - next command
     * @return - true if is not back, false if si back step
     */
    private boolean validStep(String command){
        if (old == null )
            return true;
        //
        if (command.equalsIgnoreCase("u") && old.equalsIgnoreCase("d") ||
                command.equalsIgnoreCase("d") && old.equalsIgnoreCase("u") ||
                command.equalsIgnoreCase("l") && old.equalsIgnoreCase("r") ||
                command.equalsIgnoreCase("r") && old.equalsIgnoreCase("l"))
            return false;

        return true;
    }
}

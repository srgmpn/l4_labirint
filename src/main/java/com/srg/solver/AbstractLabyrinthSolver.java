package com.srg.solver;

import com.srg.core.LabyrinthObserver;
import com.srg.model.LabyrinthModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 5/14/15.
 */
public abstract class AbstractLabyrinthSolver implements LabyrinthSolver{

    protected LabyrinthModel model;
    protected int [][] modelCopy;

    private List<LabyrinthObserver> observers = new ArrayList<LabyrinthObserver>();

    protected int nrows;
    protected int ncol;

    protected int startx;
    protected int starty;
    protected int stopx;
    protected int stopy;

    protected int [] di;
    protected int [] dj;


    protected AbstractLabyrinthSolver(LabyrinthModel model){

        this.model = model;
        this.nrows = model.getRowCount();
        this.ncol = model.getColumnCount();

        //set start and stop point
        this.startx = model.getStartCell().get("startx") + 1;
        this.starty = model.getStartCell().get("starty") + 1;
        this.stopx = model.getFinishCell().get("stopx") + 1;
        this.stopy = model.getFinishCell().get("stopy") + 1;

        modelCopy = new int[nrows + 2][ncol + 2];

        //solution coordonate
        di = new int[100];
        dj = new int[100];
    }

    /**
     * bord matrix with value -1
     */
    protected void border(){
        for (int i = 0 ; i < nrows; ++i){
            modelCopy[i][0] = -1;
            modelCopy[i][ncol+1] = -1;
        }

        for (int i = 0; i < ncol; ++i){
            modelCopy[0][i] = -1;
            modelCopy[nrows+1][i] = -1;
        }
    }

    /**
     * verif if is final position
     * @param x - curennt value row
     * @param y - curent value col
     * @return
     */
    protected boolean isFinish(int x, int y){
        return ((stopx == x) && (stopy == y));
    }

    protected void notifyAllObservers(int k){

        for (LabyrinthObserver observer : observers){
            observer.processSolution(di, dj);
        }
    }

    @Override
    public void attach(LabyrinthObserver observer){
        observers.add(observer);
    }
}

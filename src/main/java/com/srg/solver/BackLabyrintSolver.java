package com.srg.solver;

import com.srg.model.LabyrinthModel;

/**
 * Created by administrator on 5/13/15.
 */
public class BackLabyrintSolver extends AbstractLabyrinthSolver{

    int k = 0;

    public BackLabyrintSolver(LabyrinthModel model){

        super(model);

        for (int i = 0; i < nrows; i++){
            for (int j = 0; j < ncol; ++j){
                modelCopy[i+1][j+1] = model.getValueAt(i, j);
            }
        }

        super.border();
    }

    /**
     * verif if can advance next cell
     * @param i - curent row
     * @param j - curent col
     * @return - true if can, else fase
     */
    @Override
    public boolean nextCellToExplore(int i, int j){
        return  ( (i >= 1) && (j >= 1) && (i <= nrows) &&
                (j <= ncol) && (modelCopy[i][j] == 0));
    }

    @Override
    public void solve() {

        modelCopy[startx][starty] = 0;
        modelCopy[stopx][stopy] = 0;
        back(startx, starty);
    }

    /**
     * function which do backtracking
     * @param i - rows param
     * @param j - colums param
     */
    private void back(int i, int j){
        if (nextCellToExplore(i, j)){

            di[k] = i;
            dj[k] = j;
            modelCopy[i][j] = 3;
            k++;

            if (isFinish(i, j)){
                notifyAllObservers(k);

            } else {

                back(i - 1, j);
                back(i, j + 1);
                back(i + 1, j);
                back(i, j - 1);

            }
            modelCopy[i][j] = 0;
            k--;

        }
    }


}

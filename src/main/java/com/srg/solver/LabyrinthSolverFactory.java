package com.srg.solver;

import com.srg.core.Solver;
import com.srg.model.LabyrinthModel;

/**
 * Created by administrator on 5/15/15.
 */
public class LabyrinthSolverFactory {

    public static LabyrinthSolver getInstance(Solver solver, LabyrinthModel model){

        switch (solver){

            case MAN:
                return new ManualLabyrinthSolver(model);
            case BACK:
                return new BackLabyrintSolver(model);
            case LEE:
                return new LeeLabyrinthSolver(model);
        }

        return null;
    }

}
